require 'gem2deb/rake/testtask'

Gem2Deb::Rake::TestTask.new do |t|
  t.libs = ['test']
  t.test_files = FileList['test/**/test_*.rb'] - FileList['test/jruby_ssl_socket/test_pemutils.rb'] - FileList['test/test_ssl.rb']
end
